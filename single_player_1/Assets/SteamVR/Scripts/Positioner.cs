﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;

public class Positioner : MonoBehaviour
{
	// public variables
	public GameObject table; // This script is currently attached to the table itself so this isn't really necessary, but could theoretically be attached to any object and it would work the same
	public float maxPositionalError; // This is the max distance away that any tracker can be from the last table transform position and still be tracked 
	public GameObject[] pucks; // this array holds all the Vive tracker gameobjects
	public Transform[] puckRelTransforms; // in Unity the transforms of each Vive tracker relative to the table was determined and are stored here
	public Transform[] puckCalcTransforms; // these hold the transforms between each Vive tracker and the center of the table
	public Text[] HUDTexts; // these hold the on-screen text boxes that will be used in-game to display debug data about each tracker
	public SteamVR_Render steamVRRender; // this is the gateway variable to be able to access the SteamVR data about each track (namely, to find if the tracker is currently connected)
	public Material wanderingColor; // this is the color that each Vive tracker cube thingy will turn to each Update() loop when determined to be "wandering"

	// private variables
	int[] steamVRpuckIDs; // in Start(). the ID's of the Vive trackers will be determined and stored
	Material[] originalColors; // in Start(), each Vive tracker's color will be stored 

	KeyValuePair<Transform, Transform>[] puckTransformsAndOffsets;

	// Use this for initialization
	void Start()
	{
		for (int i = 0; i < pucks.Length; i++)
        {
			GameObject puck1test = GameObject.Find("The Pucks");
        }
		if (pucks.Length != puckRelTransforms.Length || pucks.Length != puckCalcTransforms.Length)
		{
			Debug.Log("Brain says: number of public variable arrays don't match!");
			// this doesn't work it seems... Application.Quit();
		}
		else
		{
			// store the puck transforms and relative puck transforms in an array of pairs for later use
			puckTransformsAndOffsets = new KeyValuePair<Transform, Transform>[pucks.Length];
			for (int i = 0; i < pucks.Length; i++)
			{
				puckTransformsAndOffsets[i] = new KeyValuePair<Transform, Transform>(pucks[i].transform, puckRelTransforms[i]);
			}
		}

		// get and store the ID of each puck 
			// make sure each puck is turned on at the start of runtime!!
		steamVRpuckIDs = new int[pucks.Length];
        for (int i = 0; i < pucks.Length; i++)
        {
			steamVRpuckIDs[i] = (int)(pucks[i].GetComponent<SteamVR_TrackedObject>().index);
			Debug.Log("Puck " + i.ToString() + " has ID: " + steamVRpuckIDs[i]);
		}

		originalColors = new Material[pucks.Length];
		for (int i = 0; i < pucks.Length; i++)
		{
			originalColors[i] = pucks[i].GetComponent<Renderer>().material;
		}
	}

	// Update is called once per frame
	void Update()
	{
		// calc the avg transforms of the pucks
		Vector3 avgPos = Vector3.zero;
		Vector4 avgQuatV4 = Vector4.zero;
		int numConnected = 0;
		for (int i = 0; i < pucks.Length; i++)
		{
			
			if (!steamVRRender.poses[steamVRpuckIDs[i]].bDeviceIsConnected)
			{
				//HUDTexts[i].text = i.ToString() + " NOT connected ";
			}
			else
			{
				numConnected++;

				KeyValuePair<Transform, Transform> transformPair = puckTransformsAndOffsets[i];
				// translational averaging
				Vector3 relPos = transformPair.Key.position - transformPair.Value.localPosition;
				puckCalcTransforms[i].position = relPos;
				// rotational averaging
				Quaternion q = (transformPair.Key.rotation) * Quaternion.Inverse(transformPair.Value.localRotation);
				// due to the double coverage problem with quaternions, we must check if the sign of the quaternion changed, if so, flip it back
				if ((Quaternion.Dot(q, puckCalcTransforms[i].rotation) < 0.0f))
					q = new Quaternion(-q.x, -q.y, -q.z, -q.w);
				puckCalcTransforms[i].rotation = q;

				// add that calculated transform to the current sum so they can be averaged later
				avgPos += relPos;
				avgQuatV4 += new Vector4(q.x, q.y, q.z, q.w);

				if ((this.transform.position - puckCalcTransforms[i].position).magnitude < maxPositionalError)
				// currently nothing is being done to use or not that tracker's data for the averaging... all it does is display on the HUD if connected, and debug logs if wandering
				{
					pucks[i].GetComponent<Renderer>().material = originalColors[i];
					//HUDTexts[i].text = i.ToString() + " is connected ";
				}
				else
				{
					pucks[i].GetComponent<Renderer>().material = wanderingColor;
					Debug.Log("Puck " + i.ToString() + " is wandering! ");
				}
			}
		}

		Debug.Log("numConnected: " + numConnected.ToString());
		if (numConnected != 0)
		{
			// divide sums by numConnected and set them to the current table transform
			avgPos /= (float)numConnected;
			avgQuatV4 /= (float)numConnected;
			table.transform.position = avgPos;
			table.transform.rotation = new Quaternion(avgQuatV4.x, avgQuatV4.y, avgQuatV4.z, avgQuatV4.w).normalized;
		}

		for (int i = 0; i < HUDTexts.Length; i++)
		{
			// put each Vive tracker's position and rotation data to the on-screen text boxes
			//HUDTexts[i].text += puckCalcTransforms[i].position.ToString() + " " + puckCalcTransforms[i].rotation.ToString();
		}
	}
}