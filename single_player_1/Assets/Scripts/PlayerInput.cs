﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInput : MonoBehaviour
{
    Color defaultColor;
    public GameObject RHand, LHand;
    public Material debugMaterial;
    //public Text text; 

    // Start is called before the first frame update
    void Start()
    {
        defaultColor = RHand.GetComponent<Renderer>().material.color;
    }

    // Update is called once per frame
    void Update()
    {
        /*
        OVRInput.Update();
        // Button.One is the "A" button
        // Button.Two is the "B" button
        // Button.Three is the "X" button
        // Button.Four is the "Y" button
        // Axis2D.PrimaryThumbstick is the left joystick
        // Axis2D.SecondaryThumbstick is the right joystick
        // Axis1D.PrimaryIndexTrigger is the left index trigger
        // Axis1D.PrimaryHandTrigger is the left hand trigger
        // Axis1D.SecondaryIndexTrigger is the right index trigger
        // Axis1D.SecondaryHandTrigger is the right hand trigger

        if (OVRInput.GetDown(OVRInput.Button.One))
        {
            text.text = "Button One Down";
            RHand.GetComponent<Renderer>().material = debugMaterial;
            LHand.GetComponent<Renderer>().material = debugMaterial;
        }
        else if(OVRInput.GetUp(OVRInput.Button.One))
        {
            text.text = "Button One Up";
            RHand.GetComponent<Renderer>().material.color = defaultColor;
            LHand.GetComponent<Renderer>().material.color = defaultColor;
        }
        else
        {
            text.text = "Nothing";
        }
        */
    }
}
