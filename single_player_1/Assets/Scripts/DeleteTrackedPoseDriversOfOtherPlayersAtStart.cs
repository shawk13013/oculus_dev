﻿using UnityEngine;
using UnityEngine.Serialization;
using Mirror;
using UnityEngine.XR;
using UnityEngine.SpatialTracking;

public class DeleteTrackedPoseDriversOfOtherPlayersAtStart : MonoBehaviour
{
    NetworkIdentity ni;

    void Start()
    {
        ni = transform.root.GetComponent<NetworkIdentity>();
        if (!ni.isLocalPlayer)
        {
            if (GetComponent<TrackedPoseDriver>())
            {
                Destroy(GetComponent<TrackedPoseDriver>());
                Debug.Log("Destroyed non-player tpd");
            }
            if (GetComponent<Camera>())
            {
                Destroy(GetComponent<Camera>());
                Debug.Log("Destroyed non-player camera");
            }
        }
    }
}