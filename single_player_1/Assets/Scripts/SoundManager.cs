﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static AudioClip sound;
    static AudioSource audioSrc;

    void Start()
    {
        sound = Resources.Load<AudioClip>("Sounds/beep");
        audioSrc = GetComponent<AudioSource>();
    }
    
    public static void PlaySound()
    {
        audioSrc.PlayOneShot(sound);
    }
}
