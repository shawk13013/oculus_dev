﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mirror
{
    public class GoOnline : MonoBehaviour
    {
        public string ipAddress = "192.168.0.179";
        NetworkManager netMan;
        public GameObject hostCube;
        public GameObject clientSphere;
        public Material debugMaterial;
        public bool isHosting;

        void Start()
        {
            netMan = GetComponent<NetworkManager>();
        }

        void Update()
        {
            if (!NetworkServer.active)
            {
                if (SystemInfo.deviceType == DeviceType.Desktop)
                {
                    if (isHosting)
                    {
                        // Hosting 
                        netMan.StartHost();
                        Debug.Log("Hosting");
                        hostCube.GetComponent<MeshRenderer>().material = debugMaterial;
                    }
                    else
                    {
                        // just serving 
                        netMan.StartServer();
                        Debug.Log("Serving");
                        hostCube.GetComponent<MeshRenderer>().material = debugMaterial;
                    }
                }
                else
                {
                    // Joining
                    netMan.StartClient();
                    netMan.networkAddress = ipAddress;
                    Debug.Log("Connecting to " + netMan.networkAddress + "...");
                    clientSphere.GetComponent<MeshRenderer>().material = debugMaterial;
                }
            }
        }
    }
}

