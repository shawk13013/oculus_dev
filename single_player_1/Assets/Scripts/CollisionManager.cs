﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionManager : MonoBehaviour
{
    public GameObject contactPointPrefab;
    public AudioSource audioSrc;
    public AudioClip sound;
    public float maxSeparationDistance;
    List<ContactPoint> contacts = new List<ContactPoint>();
    List<GameObject> contactPoints = new List<GameObject>();

    void Start()
    {
        // sound = Resources.Load<AudioClip>("Sounds/ArrowHit02");
        audioSrc = GetComponent<AudioSource>();
    }

    void AddCollisionPointObjects(Collision collision)
    {
        clearAndDestroyContactPoints();
        contacts = new List<ContactPoint>(collision.contactCount);
        int numCollisions = collision.GetContacts(contacts);
        for (int i = 0; i < numCollisions; i++)
        {
            // sometimes it marks things as a collision even before they touch, so this condition helps you set the accuracy you desire
            if (contacts[i].separation < maxSeparationDistance)
            {
                contactPoints.Add(Instantiate(contactPointPrefab, contacts[i].point, Quaternion.identity));
                //SoundManager.PlaySound();
            }
        }
    }

    void clearAndDestroyContactPoints()
    {
        contacts.Clear();
        foreach (var v in contactPoints)
        {
            Destroy(v);
        }
        contactPoints.Clear();
    }
    void OnCollisionEnter(Collision collision)
    {
        AddCollisionPointObjects(collision);
        audioSrc.PlayOneShot(sound);
    }
    void OnCollisionStay(Collision collision)
    {
        AddCollisionPointObjects(collision);
    }
    void OnCollisionExit(Collision collision)
    {
        clearAndDestroyContactPoints();
    }
}
