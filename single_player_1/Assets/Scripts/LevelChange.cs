﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChange : MonoBehaviour
{
    public int index;
    public string levelName;

    void OnTrigger(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            //Loading level with build index
            SceneManager.LoadScene(index);

            //Loading level with scene name
            SceneManager.LoadScene(levelName);

            //Restart Level
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
