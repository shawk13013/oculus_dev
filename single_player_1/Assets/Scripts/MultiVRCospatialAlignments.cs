﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiVRCospatialAlignments : MonoBehaviour
{
    GameObject[] OVRCamRigs;
    Transform[] headsetMountingPoints;
    public Transform tempChild;
    public Transform tempParent;
    public Transform[] vivePucks;
    public bool calibrate = true;

    // Start is called before the first frame update
    void Start()
    {
        // find OVRCamRigs and their vivePuckMountintPoints
        
    }

    void Update()
    {
        //Debug.Log("calibrate: " + calibrate);
        //calibrate = true;
        if (calibrate)
        {
            calibrate = !AlignAllOVRRigs(); // if it successfully does it, it will stop calibrating until turned on again
        }
    }

    bool AlignAllOVRRigs()
    {
        OVRCamRigs = GameObject.FindGameObjectsWithTag("Player");
        headsetMountingPoints = new Transform[OVRCamRigs.Length];
        for (int i = 1; i < OVRCamRigs.Length; i++) 
        {
            headsetMountingPoints[i] = OVRCamRigs[i].transform.GetChild(0).Find("CenterEyeAnchor").transform.Find("oculusHeadsetVivePuckMountingPoint");
        }

        Debug.Log("OVRCamRigs.Length: " + OVRCamRigs.Length);
        Debug.Log("headsetMountingPoints.Length: " + headsetMountingPoints.Length);
        Debug.Log("vivePucks.Length: " + vivePucks.Length);
        if (OVRCamRigs.Length == headsetMountingPoints.Length && OVRCamRigs.Length <= vivePucks.Length)
        {
            for (int i = 1; i < OVRCamRigs.Length; i++) // don't forget to sent int i = 0 if Desktop is only serving! 
            {
                alignOVRCamRigWithWorld(OVRCamRigs[i], headsetMountingPoints[i], vivePucks[i]);
            }
            return true;
        }
        else
        {
            Debug.Log("They aren't equal!");
            return false;
        }
    }

    void alignOVRCamRigWithWorld(GameObject OVRCamRig, Transform headset, Transform vivePuck)
    {
        /*
        ok new idea: Let's use unity to our advantage. We have the oculus origin, and the headset as it's origin. Let's make another object (tempParent) that is told to go where the headset is, and tell the object's child (tempChild) to go where the oculus origin is. Then we change that parent object to go where the vivePuck is, and then tell the Oculus origin to go where the child went! 
        */

        // reset transforms
        OVRCamRig.transform.position = Vector3.zero;
        OVRCamRig.transform.rotation = Quaternion.identity;

        // set tempParent to realChild (headset)
        tempParent.position = headset.position;
        tempParent.rotation = headset.rotation;
        // set tempChild to realParent (OVRCamRig)
        tempChild.position = OVRCamRig.transform.position;
        tempChild.rotation = OVRCamRig.transform.rotation;

        // set tempParent to match transform of desiredObject 
        tempParent.position = vivePuck.position;
        tempParent.rotation = vivePuck.rotation;
        // tempChild is where we want it realParent to be relative to desiredObject
        OVRCamRig.transform.position = tempChild.position;
        OVRCamRig.transform.rotation = tempChild.rotation;
    }
}
