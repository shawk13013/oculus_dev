﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendColliderInfo : MonoBehaviour
{
    public GameObject StageManagerGM;
    StageManager stageMan;

    // Start is called before the first frame update
    void Start()
    {
        stageMan = StageManagerGM.GetComponent<StageManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider col)
    {
        Debug.Log("Something collided!");
        stageMan.CollisionManager(this.gameObject, col);
    }
}
