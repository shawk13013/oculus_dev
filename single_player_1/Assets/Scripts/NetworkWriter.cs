﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace Mirror
{
    public class NetworkWriter : MonoBehaviour
    {
        public GameObject table;
        public GameObject testPuck;
        public string path;
        StreamWriter writer;

        void Start()
        {
            writer = new StreamWriter(path, true);
        }

        void Update()
        {
            GameObject[] OnlinePlayers = GameObject.FindGameObjectsWithTag("Player");
            string line = "";
            foreach (GameObject obj in OnlinePlayers)
            {
                GameObject head = obj.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject;
                line = line + head.transform.position.x + "," + head.transform.position.y + "," + head.transform.position.z + ",";
                
            }
            line = line + testPuck.transform.position.x + "," + testPuck.transform.position.y + "," + testPuck.transform.position.z;
            writer.WriteLine(line);
        }

        void OnServerDisconnect()
        {
            writer.Close();
            Debug.Log("OnServerDisconnect closed writer");
        }

        void OnStopServer()
        {
            writer.Close();
            Debug.Log("OnStopServer closed writer");
        }
    }
}
