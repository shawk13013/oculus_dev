﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mirror
{
    public class JoinNetwork : MonoBehaviour
    {
        public NetworkManager manager;

        void OnTriggerEnter(Collider collider)
        {
            if (collider.gameObject.tag == "Client")
            { 
                manager.StartClient();

                manager.networkAddress = "192.168.0.179"; //GUILayout.TextField(manager.networkAddress);//"192.168.0.179";

                // Connecting
                Debug.Log("Connecting to " + manager.networkAddress + "...");
            }
            else if (collider.gameObject.tag == "Host")
            {
                manager.StartHost();
                Debug.Log("Hosting");
            }
        }
    }
}

