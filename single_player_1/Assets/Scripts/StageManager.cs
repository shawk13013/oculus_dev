﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : MonoBehaviour
{
    public GameObject[] stages;
    int stageIndex = 0;
    public GameObject littlePucky;
    GameObject currentStage;

    // Start is called before the first frame update
    void LateStart()
    {
        if (stages.Length != 0)
        {
            currentStage = Instantiate(stages[stageIndex]);
        }
    }

    public void CollisionManager(GameObject obj, Collider col)
    {
        Debug.Log(obj.name + " was triggered by " + littlePucky.name + "!");
        if (col.name == littlePucky.name )
        {
            //Debug.Log(stageIndex);
            if (++stageIndex == stages.Length)
            {
                stageIndex = 0;
            }
            //Debug.Log("Destroying current stage");
            Destroy(currentStage);
            //Debug.Log("Creating next stage");
            currentStage = Instantiate(stages[stageIndex]);
        }
    }
}
